<?php

namespace mvc\controllers;

use mvc\core\Controller;
use mvc\models\Category as CategoryModel;
use mvc\models\Product as ProductModel;

class Category extends Controller
{

    public $category;

    public function __construct()
    {
        $this->category = new CategoryModel();
        $this->product = new ProductModel();
    }

    /**
     * add new category
     */

    public function add()
    {
        $this->category->setName($_POST['name']);
        $this->category->add();
        $this->redirect("Location: http://localhost/mvc/public/category/addform");

    }

    /**
     * delete  category
     */
    public function delete()
    {
        $this->category->setId($_POST['id']);
        $this->category->delete();

        $this->redirect("Location: http://localhost/mvc/public/category/list");
    }

    /**
     * Shows category list
     */
    public function showCategory()
    {
        //category load
        $category= $this->category->load();
        //layout render
        $this->view('category/list', $data = ['category' => $category]);
    }

    /**
     * Shows add category form
     */
    public function Categoryform()
    {
        $category= $this->category->load();
        $this->view('category/addform', $data = ['category' => $category]);
    }

    /**
     * Shows  category at views
     */
    public function Categorydisplay()
    {
        $url = $this->parseUrl($_GET['url']);

        $category = $this->category->load();

        $product = $this->product->loadByCategoryId($url[3]);

        $this->header();

        $this->view('store/category', $data = ['category' => $category, 'product' => $product]);
    }





}
